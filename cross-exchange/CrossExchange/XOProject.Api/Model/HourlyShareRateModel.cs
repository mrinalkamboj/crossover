﻿using System;
using System.ComponentModel.DataAnnotations;

namespace XOProject.Api.Model
{
    public class HourlyShareRateModel:IEquatable<HourlyShareRateModel>
    {
        public int Id { get; set; }

        [Required]
        public DateTime TimeStamp { get; set; }

        [Required]
        [RegularExpression("^[A-Z]{3}$", ErrorMessage = "Share symbol should be all capital letters with 3 characters")]
        public string Symbol { get; set; }

        [Required]
        public decimal Rate { get; set; }

        public bool Equals(HourlyShareRateModel hourlyShareRateModel)
        {
            return hourlyShareRateModel.Id.Equals(Id)
                   && hourlyShareRateModel.TimeStamp.Equals(TimeStamp)
                   && hourlyShareRateModel.Symbol.Equals(Symbol)
                   && hourlyShareRateModel.Rate.Equals(Rate);
        }

        public override bool Equals(object hourlyShareRateModel)
        {
            return Equals(hourlyShareRateModel as HourlyShareRateModel);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Id.GetHashCode();
                hash = hash * 23 + TimeStamp.GetHashCode();
                hash = hash * 23 + Symbol.GetHashCode();
                hash = hash * 23 + Rate.GetHashCode();

                return hash;
            }
        }
    }
}
