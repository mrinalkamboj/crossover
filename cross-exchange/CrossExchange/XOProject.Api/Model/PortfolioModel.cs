﻿using System;

namespace XOProject.Api.Model
{
    public class PortfolioModel :IEquatable<PortfolioModel>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Equals(PortfolioModel portfolioModel)
        {
            return portfolioModel.Id.Equals(Id)
                   && portfolioModel.Name.Equals(Name);
        }

        public override bool Equals(object portfolioModel)
        {
            return Equals(portfolioModel as PortfolioModel);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Id.GetHashCode();
                hash = hash * 23 + Name.GetHashCode();

                return hash;
            }
        }
    }
}
