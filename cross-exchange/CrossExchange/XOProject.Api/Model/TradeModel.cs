﻿using System;
using System.ComponentModel.DataAnnotations;

namespace XOProject.Api.Model
{
    public class TradeModel : IEquatable<TradeModel>
    {
        [Required]
        public string Symbol { get; set; }

        [Required]
        public int NoOfShares { get; set; }

        [Required]
        public int PortfolioId { get; set; }

        public string Action { get; set; }

        public bool Equals(TradeModel tradeModel)
        {
            return tradeModel.Symbol.Equals(Symbol)
                   && tradeModel.NoOfShares.Equals(NoOfShares)
                   && tradeModel.PortfolioId.Equals(PortfolioId)
                   && tradeModel.Action.Equals(Action);
        }

        public override bool Equals(object tradeModel)
        {
            return Equals(tradeModel as TradeModel);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Symbol.GetHashCode();
                hash = hash * 23 + NoOfShares.GetHashCode();
                hash = hash * 23 + PortfolioId.GetHashCode();
                hash = hash * 23 + Action.GetHashCode();

                return hash;
            }
        }
    }
}