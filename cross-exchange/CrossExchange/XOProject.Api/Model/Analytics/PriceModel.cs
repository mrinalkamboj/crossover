﻿using System;

namespace XOProject.Api.Model.Analytics
{
    public class PriceModel : IEquatable<PriceModel>
    {
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }

        public bool Equals(PriceModel hourlyShareRateModel)
        {
            return hourlyShareRateModel.Open.Equals(Open)
                   && hourlyShareRateModel.High.Equals(High)
                   && hourlyShareRateModel.Low.Equals(Low)
                   && hourlyShareRateModel.Close.Equals(Close);
        }

        public override bool Equals(object hourlyShareRateModel)
        {
            return Equals(hourlyShareRateModel as PriceModel);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Open.GetHashCode();
                hash = hash * 23 + High.GetHashCode();
                hash = hash * 23 + Low.GetHashCode();
                hash = hash * 23 + Close.GetHashCode();

                return hash;
            }
        }
    }
}
