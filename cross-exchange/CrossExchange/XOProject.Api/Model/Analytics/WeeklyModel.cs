﻿using System;

namespace XOProject.Api.Model.Analytics
{
    public class WeeklyModel : IEquatable<WeeklyModel>
    {
        public int Year { get; set; }
        public int Week { get; set; }
        public string Symbol { get; set; }
        public PriceModel Price { get; set; }

        public bool Equals(WeeklyModel hourlyShareRateModel)
        {
            return hourlyShareRateModel.Symbol.Equals(Symbol)
                   && hourlyShareRateModel.Year.Equals(Year)
                   && hourlyShareRateModel.Week.Equals(Week)
                   && hourlyShareRateModel.Price.Equals(Price);
        }

        public override bool Equals(object hourlyShareRateModel)
        {
            return Equals(hourlyShareRateModel as WeeklyModel);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Symbol.GetHashCode();
                hash = hash * 23 + Year.GetHashCode();
                hash = hash * 23 + Week.GetHashCode();
                hash = hash * 23 + Price.GetHashCode();

                return hash;
            }
        }
    }
}