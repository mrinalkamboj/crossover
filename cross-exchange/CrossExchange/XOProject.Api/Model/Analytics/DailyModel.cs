﻿using System;

namespace XOProject.Api.Model.Analytics
{
    public class DailyModel : IEquatable<DailyModel>
    {
        public string Symbol { get; set; }
        public DateTime Day { get; set; }
        public PriceModel Price { get; set; }

        public bool Equals(DailyModel hourlyShareRateModel)
        {
            return hourlyShareRateModel.Symbol.Equals(Symbol)
                   && hourlyShareRateModel.Day.Equals(Day)
                   && hourlyShareRateModel.Price.Equals(Price);
        }

        public override bool Equals(object hourlyShareRateModel)
        {
            return Equals(hourlyShareRateModel as DailyModel);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Symbol.GetHashCode();
                hash = hash * 23 + Day.GetHashCode();
                hash = hash * 23 + Price.GetHashCode();

                return hash;
            }
        }
    }
}
