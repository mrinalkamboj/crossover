﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using XOProject.Api.Model.Analytics;
using XOProject.Services.Domain;
using XOProject.Services.Exchange;

using Microsoft.AspNetCore.Mvc;

namespace XOProject.Api.Controller
{
    [Route("api")]
    public class AnalyticsController : ControllerBase
    {
        /// <summary>
        /// Analytics Service
        /// </summary>
        private readonly IAnalyticsService _analyticsService;

        /// <summary>
        /// AnalyticsController Constructor
        /// </summary>
        /// <param name="analyticsService"></param>
        public AnalyticsController(IAnalyticsService analyticsService)
        {
            _analyticsService = analyticsService;
        }

        /// <summary>
        /// Daily Analytics Service
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        [HttpGet("daily/{symbol}/{year}/{month}/{day}")]
        public async Task<IActionResult> Daily([FromRoute] string symbol, [FromRoute] int year, [FromRoute] int month, [FromRoute] int day)
        {
            // Fetch the Daily Price using the Analytics Service
            var dailyAnalyticsPrice = await _analyticsService.GetDailyAsync(symbol, new DateTime(year,month,day));
            
            // Transform the Result to the DailyModel entity
            var result = new DailyModel
            {
                Symbol = symbol,
                Day = new DateTime(year, month, day),
                Price = Map(dailyAnalyticsPrice)
            };

            // Return the Result
            return Ok(result);
        }

        /// <summary>
        /// Weekly Analytics Service
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="year"></param>
        /// <param name="week"></param>
        /// <returns></returns>
        [HttpGet("weekly/{symbol}/{year}/{week}")]
        public async Task<IActionResult> Weekly([FromRoute] string symbol, [FromRoute] int year, [FromRoute] int week)
        {
            // Fetch the Weekly Price using the Analytics Service
            var weeklyAnalyticsPrice = await _analyticsService.GetWeeklyAsync(symbol, year,week);

            // Transform the Result to the WeeklyModel entity
            var result = new WeeklyModel
            {
                Symbol = symbol,
                Year = year,
                Week = week,
                Price = Map(weeklyAnalyticsPrice)
            };

            // Return the Result
            return Ok(result);
        }

        /// <summary>
        /// Monthly Analytics Service
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet("monthly/{symbol}/{year}/{month}")]
        public async Task<IActionResult> Monthly([FromRoute] string symbol, [FromRoute] int year, [FromRoute] int month)
        {
            // Fetch the Weekly Price using the Analytics Service
            var monthlyAnalyticsPrice = await _analyticsService.GetMonthlyAsync(symbol, year, month);

            // Transform the Result to the MonthlyModel entity
            var result = new MonthlyModel()
            {
                Symbol = symbol,
                Year = year,
                Month = month,
                Price = Map(monthlyAnalyticsPrice)
            };

            // Return the Result
            return Ok(result);
        }

        /// <summary>
        /// AnalyticsPrice to the PriceModel mapping
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        private PriceModel Map(AnalyticsPrice price)
        {
            return new PriceModel()
            {
                Open = price.Open,
                Close = price.Close,
                High = price.High,
                Low = price.Low
            };
        }
    }
}