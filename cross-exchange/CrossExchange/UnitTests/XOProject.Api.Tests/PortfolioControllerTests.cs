using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using XOProject.Api.Controller;
using XOProject.Api.Model;
using XOProject.Repository.Domain;
using XOProject.Services.Exchange;
using XOProject.Helpers;

namespace XOProject.Api.Tests
{
    /// <summary>
    /// Share Controller Unit Tests
    /// </summary>
    public class PortfolioControllerTests
    {
        /// <summary>
        ///  Portfolio Service Mock Object
        /// </summary>
        private readonly Mock<IPortfolioService> _portfolioServiceMock = new Mock<IPortfolioService>();

        /// <summary>
        /// Portfolio Controller
        /// </summary>
        private readonly PortfolioController _portfolioController;

        /// <summary>
        /// Portfolio Controller Test Constructor
        /// </summary>
        public PortfolioControllerTests()
        {
            _portfolioController = new PortfolioController(_portfolioServiceMock.Object);
        }

        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _portfolioServiceMock.Reset();
        }

        /// <summary>
        /// Get PortFolioNotFound Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Get_PortFolioNotFound_Test()
        {
            // Arrange

            // Symbol Value For Filtering
            const int inValidPortFolioId = -1;

            // Mock Service object Return Setup
            _portfolioServiceMock.Setup( mock => mock.GetByIdAsync(inValidPortFolioId))
                            .ReturnsAsync(() => null);

            // Act
            var result = await _portfolioController.GetPortfolio(inValidPortFolioId);
            
            var finalNotFoundResult = result as NotFoundResult;
         
            Assert.NotNull(finalNotFoundResult); // Final result not null Assert
            Assert.IsAssignableFrom<NotFoundResult>(finalNotFoundResult);
        }

        /// <summary>
        /// Get PortFolioNotFound Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Get_PortFolioFound_Test()
        {
            // Arrange

            // Symbol Value For Filtering
            const int validPortFolioId = 1;

            var portfolio = new Portfolio()
            {
                Id = 1,
                Name = "Mrinal",
                Trades = null
            };

            // Mock Service object Return Setup
            _portfolioServiceMock.Setup(mock => mock.GetByIdAsync(validPortFolioId))
                                 .ReturnsAsync(() => portfolio);

            // Act
            var result = await _portfolioController.GetPortfolio(validPortFolioId);

            // Assert
            Assert.NotNull(result);// Controller Result Not Null Assert

            var finalObjectResult = result as OkObjectResult;
            var finalResult = finalObjectResult?.Value as PortfolioModel;
            Assert.NotNull(finalResult);
            Assert.AreEqual(ModelConversion.Map(portfolio),finalResult); // Final result not null Assert
        }

        /// <summary>
        /// Get PortFolioNotFound Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Post_SuccessfullyInsertPortfolio_Test()
        {
            // Arrange
            var portfolioModel = new PortfolioModel()
            {
                Id = 100,
                Name = "UnitTest"
            };

            //// Mock Service object Return Setup
            //_portfolioServiceMock.Setup(mock => mock.GetByIdAsync(validPortFolioId))
            //    .ReturnsAsync(() => portfolio);

            // Act
            var result = await _portfolioController.Post(portfolioModel);

            // Assert
            Assert.NotNull(result);// Controller Result Not Null Assert

            var finalCreatedResult = result as CreatedResult;
            Assert.NotNull(finalCreatedResult);
            Assert.AreEqual(portfolioModel, finalCreatedResult.Value); // Final result not null Assert
        }


    }
}
