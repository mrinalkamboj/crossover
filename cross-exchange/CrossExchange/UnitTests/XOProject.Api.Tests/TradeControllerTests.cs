using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using XOProject.Api.Controller;
using XOProject.Api.Model;
using XOProject.Services.Exchange;
using XOProject.Helpers;

namespace XOProject.Api.Tests
{
    /// <summary>
    /// Share Controller Unit Tests
    /// </summary>
    public class TradeControllerTests
    {
        /// <summary>
        ///  Share Service Mock Object
        /// </summary>
        private readonly Mock<ITradeService> _tradeServiceMock = new Mock<ITradeService>();

        /// <summary>
        /// Share Controller
        /// </summary>
        private readonly TradeController _tradeController;

        public TradeControllerTests()
        {
            _tradeController = new TradeController(_tradeServiceMock.Object);
        }

        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _tradeServiceMock.Reset();
        }

        /// <summary>
        /// Get HourlyShareRateForSymbol Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Get_AllTradings_Test()
        {
            // Arrange

            // Symbol Value For Filtering
            const int portFolioId = 1;

            // Create Data HourlyShareRateModel
            var tradeModel1 = new TradeModel { Symbol = "CBI", NoOfShares = 100,Action = "BUY",PortfolioId = 1};

            var tradeModel2 = new TradeModel { Symbol = "CBI", NoOfShares = 100, Action = "BUY", PortfolioId = 1 };

            var tradeModel3 = new TradeModel { Symbol = "REL", NoOfShares = 100, Action = "SELL", PortfolioId = 1 };

            var tradeModel4 = new TradeModel { Symbol = "REL", NoOfShares = 100, Action = "SELL", PortfolioId = 1 };

            // Prepare Model Data
            var tradeModelList = new List<TradeModel> {tradeModel1, tradeModel2, tradeModel3, tradeModel4};
            
            // Create HourlyShareRate List from  HourlyShareRateModel List
            var tradeList = ModelConversion.Map(tradeModelList);

            // Mock Service object Return Setup
           _tradeServiceMock.Setup( mock => mock.GetByPortfolioId(portFolioId))
                            .ReturnsAsync(() => tradeList.ToList());

            // Act
            var resultList = await _tradeController.GetAllTradings(portFolioId);

            // Assert
            Assert.NotNull(resultList);// Controller Result Not Null Assert

            var finalOkObjectResult = resultList as OkObjectResult;
            var finalResultList = finalOkObjectResult?.Value as List<TradeModel>;
            Assert.NotNull(finalResultList); // Final result not null Assert
            Assert.IsTrue(ListHelper.CompareList(tradeModelList, finalResultList)); // Assert True
        }

        /// <summary>
        /// Insert hourly share rate data
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Post_ShouldInsertTradeModel_Test()
        {
            // Arrange
            var tradeModel = new TradeModel { Symbol = "CBI", NoOfShares = 100, Action = "BUY", PortfolioId = 1 };

            // Mock Service object Return Setup
            _tradeServiceMock.Setup(mock => mock.BuyOrSell(tradeModel.PortfolioId, tradeModel.Symbol, tradeModel.NoOfShares, tradeModel.Action))
                .ReturnsAsync(() => ModelConversion.Map(tradeModel));

            // Act
            var result = await _tradeController.Post(tradeModel);

            // Assert
            Assert.NotNull(result);

            // TODO: This unit test is broken, the result received from the Post method is correct.
            var createdResult = result as CreatedResult;
            Assert.NotNull(createdResult);
            Assert.AreEqual(tradeModel, createdResult.Value); // Bug-Fix (Compared the Assert with CreatedResult status code)
        }


    }
}
