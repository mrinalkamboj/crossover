using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using XOProject.Api.Controller;
using XOProject.Api.Model;
using XOProject.Services.Exchange;
using XOProject.Helpers;

namespace XOProject.Api.Tests
{
    /// <summary>
    /// Share Controller Unit Tests
    /// </summary>
    public class ShareControllerTests
    {
        /// <summary>
        ///  Share Service Mock Object
        /// </summary>
        private readonly Mock<IShareService> _shareServiceMock = new Mock<IShareService>();

        /// <summary>
        /// Share Controller
        /// </summary>
        private readonly ShareController _shareController;

        public ShareControllerTests()
        {
            _shareController = new ShareController(_shareServiceMock.Object);
        }

        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _shareServiceMock.Reset();
        }

        /// <summary>
        /// Get HourlyShareRateForSymbol Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Get_HourlyShareRateForSymbol_Test()
        {
            // Arrange

            // Symbol Value For Filtering
            const string symbolDataValue = "REL";

            // Create Data HourlyShareRateModel
            var hourRateModel1 = new HourlyShareRateModel { Symbol = "CBI", Rate = 330.0M, TimeStamp = new DateTime(2018, 08, 17, 0, 0, 0)};

            var hourRateModel2 = new HourlyShareRateModel { Symbol = "CBI", Rate = 350.0M, TimeStamp = new DateTime(2018, 08, 18, 0, 0, 0)};

            var hourRateModel3 = new HourlyShareRateModel { Symbol = "REL", Rate = 150.0M, TimeStamp = new DateTime(2018, 08, 17, 0, 0, 0)};

            var hourRateModel4 = new HourlyShareRateModel { Symbol = "REL",Rate = 160.0M,TimeStamp = new DateTime(2018, 08, 18, 0, 0, 0)};

            // Prepare Model Data
            var hourlyShareRateModelList = new List<HourlyShareRateModel> {hourRateModel1, hourRateModel2, hourRateModel3, hourRateModel4};
            
            // Create HourlyShareRate List from  HourlyShareRateModel List
            var hourlyShareRateList = ModelConversion.Map(hourlyShareRateModelList);

            // Mock Service object Return Setup
           _shareServiceMock.Setup( mock => mock.GetBySymbolAsync(symbolDataValue))
                            .ReturnsAsync(()=>hourlyShareRateList.Where(x => x.Symbol.Equals(symbolDataValue))
                                                                 .ToList());

            // Act
            var resultList = await _shareController.Get(symbolDataValue);

            // Assert
            Assert.NotNull(resultList);// Controller Result Not Null Assert

            var finalOkObjectResult = resultList as OkObjectResult;
            var finalResultList = finalOkObjectResult?.Value as List<HourlyShareRateModel>;
            Assert.NotNull(finalResultList); // Final result not null Assert
            Assert.IsTrue(ListHelper.CompareList(hourlyShareRateModelList.Where(x => x.Symbol.Equals(symbolDataValue)).ToList(), finalResultList)); // Assert True
        }

        /// <summary>
        /// Insert hourly share rate data
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Post_ShouldInsertHourlySharePrice_Test()
        {
            // Arrange
            var hourRate = new HourlyShareRateModel
            {
                Symbol = "CBI",
                Rate = 330.0M,
                TimeStamp = new DateTime(2018, 08, 17, 5, 0, 0)
            };
            
            // Act
            var result = await _shareController.Post(hourRate);

            // Assert
            Assert.NotNull(result);

            // TODO: This unit test is broken, the result received from the Post method is correct.
            var createdResult = result as CreatedResult;
            Assert.NotNull(createdResult);
            Assert.AreEqual(201, createdResult.StatusCode); // Bug-Fix (Compared the Assert with CreatedResult status code)
        }

        /// <summary>
        ///  Fetch the latest price for unknown symbol
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Get_LatestPriceForUnknownSymbol_Test()
        {
            // Arrange
            // Symbol Value For Filtering
            const string symbolDataValue = "UNK";

            // Act
            var resultList = await _shareController.GetLatestPrice(symbolDataValue);

            // Assert
            var finalOkObjectResult = resultList as OkObjectResult;
            Assert.AreEqual(0.00M,finalOkObjectResult?.Value);
        }

        /// <summary>
        /// Fetch the latest price for known symbol
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Get_LatestPriceForKnownSymbol_Test()
        {
            // Arrange
            // Symbol Value For Filtering
            const string symbolDataValue = "REL";

            // Act
            var resultList = await _shareController.GetLatestPrice(symbolDataValue);

            // Assert
            var finalOkObjectResult = resultList as OkObjectResult;
            Assert.AreEqual(200, finalOkObjectResult?.StatusCode);
        }
    }
}
