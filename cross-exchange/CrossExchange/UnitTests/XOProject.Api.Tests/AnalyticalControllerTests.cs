using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using XOProject.Api.Controller;
using XOProject.Api.Model.Analytics;
using XOProject.Services.Exchange;
using XOProject.Helpers;
using XOProject.Services.Domain;

namespace XOProject.Api.Tests
{
    /// <summary>
    /// Share Controller Unit Tests
    /// </summary>
    public class AnalyticalControllerTests
    {
        /// <summary>
        ///  Share Service Mock Object
        /// </summary>
        private readonly Mock<IAnalyticsService> _analyticsServiceMock = new Mock<IAnalyticsService>();

        /// <summary>
        /// Share Controller
        /// </summary>
        private readonly AnalyticsController _analyticsController;

        public AnalyticalControllerTests()
        {
            _analyticsController = new AnalyticsController(_analyticsServiceMock.Object);
        }

        /// <summary>
        /// Object Teardown
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _analyticsServiceMock.Reset();
        }

        /// <summary>
        /// Get HourlyShareRateForSymbol Test
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Get_DailyPrice_Test()
        {
            // Arrange

            // Symbol Value For Filtering
            const string symbolDataValue = "REL";
            var dateTimeDataValue = new DateTime(2019,12,21);

            // Create Data HourlyShareRateModel
            var dailyAnalyticsPrice = new AnalyticsPrice
            {
                Low = 1,
                High = 5,
                Open = 2,
                Close = 4
            };

            var testDailyModel = new DailyModel
            {
                Symbol = symbolDataValue,
                Day = new DateTime(2019, 12, 21),
                Price = ModelConversion.Map(dailyAnalyticsPrice)
            };

            // Mock Service object Return Setup
            _analyticsServiceMock.Setup( m => m.GetDailyAsync(symbolDataValue, dateTimeDataValue))
                            .ReturnsAsync(() => dailyAnalyticsPrice);

            // Act
            var result = await _analyticsController.Daily(symbolDataValue,2019,12,21);

            // Assert
            Assert.NotNull(result);// Controller Result Not Null Assert

            var finalOkObjectResult = result as OkObjectResult;
            var finalResult = finalOkObjectResult?.Value as DailyModel;
            Assert.NotNull(finalResult); // Final result not null Assert
            Assert.AreEqual(testDailyModel, finalResult); // Assert True
        }

        [Test]
        public async Task Get_WeeklyPrice_Test()
        {
            // Arrange

            // Symbol Value For Filtering
            const string symbolDataValue = "REL";
            var dateTimeDataValue = new DateTime(2019, 12, 21);

            // Create Data HourlyShareRateModel
            var weeklyAnalyticsPrice = new AnalyticsPrice
            {
                Low = 1,
                High = 5,
                Open = 2,
                Close = 4
            };

            var testWeeklyModel = new WeeklyModel
            {
                Year = 2019,
                Week = 33,
                Symbol = symbolDataValue,
                Price = ModelConversion.Map(weeklyAnalyticsPrice)
            };

            // Mock Service object Return Setup
            _analyticsServiceMock.Setup(m => m.GetWeeklyAsync(symbolDataValue,2019,33))
                .ReturnsAsync(() => weeklyAnalyticsPrice);

            // Act
            var result = await _analyticsController.Weekly(symbolDataValue, 2019, 33);

            // Assert
            Assert.NotNull(result);// Controller Result Not Null Assert

            var finalOkObjectResult = result as OkObjectResult;
            var finalResult = finalOkObjectResult?.Value as WeeklyModel;
            Assert.NotNull(finalResult); // Final result not null Assert
            Assert.AreEqual(testWeeklyModel, finalResult); // Assert True
        }


    }
}
