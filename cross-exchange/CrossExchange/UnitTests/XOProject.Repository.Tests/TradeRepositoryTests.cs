﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Moq;
using NUnit.Framework;

using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Repository.Tests.Helpers;

namespace XOProject.Repository.Tests
{
    public class TradeRepositoryTests
    {
        private TradeRepository _tradeRepository;
        private Mock<ExchangeContext> _mockContext;

        [SetUp]
        public void Initialize()
        {
           // Mock the ExchangeContext and supply as a constructor parameter to the TradeRepository
           var options = new DbContextOptionsBuilder<ExchangeContext>()
                .UseInMemoryDatabase("ExchangeUnitTestsDb")
                .Options;
            _mockContext = new Mock<ExchangeContext>(options,new DataSeed());
            _tradeRepository = new TradeRepository(_mockContext.Object);
        }

        [TearDown]
        public void Cleanup()
        {
            _tradeRepository = null;
            _mockContext.Object.Dispose();
            _mockContext = null;
        }

        [Test]
        public async Task GetAsync_WhenExists_ReturnsTrade()
        {
            // Arrange
            const int tradeId = 10;

            var expectedTrade = new Trade
            {
                Id = 1,
                Symbol = "REL",
                NoOfShares = 50,
                Action = "BUY",
                PortfolioId = 1,
                ContractPrice = 5000.00M
            };

            // Mock Setup
            _mockContext.Setup(mock => mock.FindAsync<Trade>(tradeId))
                .ReturnsAsync(() => expectedTrade);

            // Act
            var result = await _tradeRepository.GetAsync(tradeId);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(result,expectedTrade);
        }

        [Test]
        public async Task QueryAsync_WhenNotExists_ReturnsNull()
        {
            // Arrange
            var tradeList = new List<Trade>
            {
                new Trade
                {
                    Id = 1,
                    Symbol = "REL",
                    NoOfShares = 50,
                    Action = "BUY",
                    PortfolioId = 1,
                    ContractPrice = 5000.00M
                }
            };

            // Mock Setup (Returns null value for DBSet<Trade>)
            _mockContext.Setup(mock => mock.Set<Trade>())
                .Returns(() => null);

            // Act
            var result = await Task.Run(() => _tradeRepository.Query());

            // Assert (value is null)
            Assert.IsNull(result);
            //Assert.IsTrue(ListHelper.CompareList(tradeList, result.ToList()));
        }
    }
}
