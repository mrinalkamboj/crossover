﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using XOProject.Helpers;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Services.Domain;
using XOProject.Services.Exchange;
using XOProject.Services.Tests.Helpers;

namespace XOProject.Services.Tests
{
    /// <summary>
    /// Trade Service Unit Test Cases
    /// </summary>
    public class TradeServiceTests
    {
        /// <summary>
        /// ITradeRepository Mock Object
        /// </summary>
        private readonly Mock<ITradeRepository> _tradeRepositoryMock = new Mock<ITradeRepository>();

        /// <summary>
        /// IShareService Mock Object
        /// </summary>
        private readonly Mock<IShareService> _shareServiceMock = new Mock<IShareService>();

        /// <summary>
        /// 
        /// </summary>
        private readonly TradeService _tradeService;

        /// <summary>
        /// 
        /// </summary>
        public TradeServiceTests()
        {
            _tradeService = new TradeService(_tradeRepositoryMock.Object, _shareServiceMock.Object);
        }

        /// <summary>
        /// 
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _tradeRepositoryMock.Reset();
            _shareServiceMock.Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Trade_GetPortfolioId_Test()
        {
            // Arrange
            const int portfolioId = 1;
            var tradeList = ArrangeTrades();

            // Mock object Return
            _tradeRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<Trade>(tradeList));

            // Act
            var queryResult = await _tradeService.GetByPortfolioId(portfolioId);

            // Assert
            Assert.NotNull(queryResult);
            Assert.IsTrue(ListHelper.CompareList(queryResult.ToList(), tradeList.ToList()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Trade_GetBuyOrSell_Test()
        {
            // Arrange
            const int id = 1;
            const int portfolioId = 1;
            const int noofshares = 100;
            const string symbol = "CBI";
            const string action = "BUY";
            var dateTime = new DateTime(2018,08,18);

            var hourlyShareRate = new HourlyShareRate
            {
                TimeStamp = dateTime,
                Symbol = symbol,
                Rate = 100.00M,
                Id = id
            };

            var trade = new Trade()
            {
                Action = action,
                PortfolioId = portfolioId,
                Symbol = symbol,
                NoOfShares = noofshares,
                ContractPrice = hourlyShareRate.Rate * noofshares
            };

            // Mock object Return
            _shareServiceMock
                .Setup(mock => mock.GetLastPriceAsync(symbol))
                .ReturnsAsync(() => hourlyShareRate);

            // Act
            var queryResult = await _tradeService.BuyOrSell(portfolioId,symbol, noofshares,action);

            // Assert
            Assert.NotNull(queryResult);
            Assert.AreEqual(queryResult, trade);
        }

        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<Trade> ArrangeTrades()
        {
            var trades = new[]
            {
                new Trade
                {
                    Id = 1,
                    Symbol = "CBI",
                    NoOfShares = 100,
                    Action = "BUY",
                    PortfolioId = 1,
                    ContractPrice = 100.00M
                },
                new Trade
                {
                    Id = 2,
                    Symbol = "REL",
                    NoOfShares = 200,
                    Action = "SELL",
                    PortfolioId = 1,
                    ContractPrice = 200.00M
                }
            };

            return trades.ToList();
        }
    }
}
