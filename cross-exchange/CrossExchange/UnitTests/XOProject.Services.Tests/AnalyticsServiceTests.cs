﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using XOProject.Helpers;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Services.Domain;
using XOProject.Services.Exchange;
using XOProject.Services.Tests.Helpers;

namespace XOProject.Services.Tests
{
    public class AnalyticsServiceTests
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Mock<IShareRepository> _shareRepositoryMock = new Mock<IShareRepository>();

        /// <summary>
        /// 
        /// </summary>
        private readonly AnalyticsService _analyticsService;

        /// <summary>
        /// 
        /// </summary>
        public AnalyticsServiceTests()
        {
            _analyticsService = new AnalyticsService(_shareRepositoryMock.Object);
        }

        /// <summary>
        /// 
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _shareRepositoryMock.Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetDailyAsyncForShareAndDateTime()
        {
            // Arrange
            var rateList = ArrangeRates();
            const string symbol = "CBI";
            var dateTime = new DateTime(2018, 08, 17, 5, 10, 15);
            var analyticsPriceResult = new AnalyticsPrice
            {
                Low = 305.0M,
                High = 320.0M,
                Close = 320.0M,
                Open = 310.0M
            };

            // Mock object Return
            _shareRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<HourlyShareRate>(rateList));

            // Act
            var queryResult = await _analyticsService.GetDailyAsync(symbol, dateTime);

            // Assert
            Assert.NotNull(queryResult);
            Assert.AreEqual(queryResult, analyticsPriceResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetWeeklyAsyncForShareAndWeekOfYear()
        {
            // Arrange
            var rateList = ArrangeRates();
            const string symbol = "CBI";
            var analyticsPriceResult = new AnalyticsPrice
            {
                Low = 305.0M,
                High = 320.0M,
                Close = 320.0M,
                Open = 310.0M
            };

            // Mock object Return
            _shareRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<HourlyShareRate>(rateList));

            // Act
            var queryResult = await _analyticsService.GetWeeklyAsync(symbol,2018,33);

            // Assert
            Assert.NotNull(queryResult);
            Assert.AreEqual(queryResult, analyticsPriceResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetMonthlyAsyncForShareAndDateTime()
        {
            // Arrange
            var rateList = ArrangeRates();
            const string symbol = "CBI";
            var dateTime = new DateTime(2018, 08, 17, 5, 10, 15);
            var analyticsPriceResult = new AnalyticsPrice
            {
                Low = 305.0M,
                High = 320.0M,
                Close = 320.0M,
                Open = 310.0M
            };

            // Mock object Return
            _shareRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<HourlyShareRate>(rateList));

            // Act
            var queryResult = await _analyticsService.GetMonthlyAsync(symbol,dateTime.Year,dateTime.Month);

            // Assert
            Assert.NotNull(queryResult);
            Assert.AreEqual(queryResult, analyticsPriceResult);
        }


        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<HourlyShareRate> ArrangeRates()
        {
            var rates = new[]
            {
                new HourlyShareRate
                {
                    Id = 1,
                    Symbol = "CBI",
                    Rate = 310.0M,
                    TimeStamp = new DateTime(2018, 08, 17, 9, 0, 0)
                },
                new HourlyShareRate
                {
                    Id = 1,
                    Symbol = "CBI",
                    Rate = 320.0M,
                    TimeStamp = new DateTime(2018, 08, 17, 12, 0, 0)
                },
                new HourlyShareRate
                {
                    Id = 1,
                    Symbol = "CBI",
                    Rate = 305.0M,
                    TimeStamp = new DateTime(2018, 08, 17, 13, 0, 0)
                },
                new HourlyShareRate
                {
                    Id = 1,
                    Symbol = "CBI",
                    Rate = 320.0M,
                    TimeStamp = new DateTime(2018, 08, 17, 16, 0, 0)
                }

            };

            return rates.ToList();
        }
    }
}
