﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using XOProject.Helpers;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Services.Exchange;
using XOProject.Services.Tests.Helpers;

namespace XOProject.Services.Tests
{
    public class ShareServiceTests
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Mock<IShareRepository> _shareRepositoryMock = new Mock<IShareRepository>();

        /// <summary>
        /// 
        /// </summary>
        private readonly ShareService _shareService;

        /// <summary>
        /// 
        /// </summary>
        public ShareServiceTests()
        {
            _shareService = new ShareService(_shareRepositoryMock.Object);
        }

        /// <summary>
        /// 
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            _shareRepositoryMock.Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetHourlyAsync_WhenExists_GetsHourlySharePrice()
        {
            // Arrange
            var rateList = ArrangeRates();
            const string symbol = "CBI";
            var dateTime = new DateTime(2018, 08, 17, 5, 10, 15);

            // Mock object Return
            _shareRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<HourlyShareRate>(rateList));

            // Act
            var result = await _shareService.GetHourlyAsync(symbol, dateTime);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(4, result.Id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetBySymbolAsync_WhenExists_GetsHourlySharePrice()
        {
            // Arrange
            var rateList = ArrangeRates();
            const string symbol = "CBI";

            // Mock object Return
            _shareRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<HourlyShareRate>(rateList));

            // Act
            var result = await _shareService.GetBySymbolAsync(symbol);

            var comparisonListFirst = rateList.Where(x => x.Symbol.Equals(symbol)).Distinct().ToList();

            var comparisonListSecond = result.Where(x => x.Symbol.Equals(symbol)).Distinct().ToList();

            // Assert
            Assert.NotNull(result);
            Assert.IsTrue(ListHelper.CompareList(comparisonListFirst, comparisonListSecond));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetLastPriceAsync_WhenExists_GetsHourlySharePrice()
        {
            // Arrange
            var rateList = ArrangeRates();
            const string symbol = "CBI";

            // Mock object Return
            _shareRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<HourlyShareRate>(rateList));

            // Act
            var result = await _shareService.GetLastPriceAsync(symbol);

            var value = rateList.Where(x => x.Symbol.Equals(symbol))
                                             .Distinct()
                                             .OrderByDescending(x => x.TimeStamp)
                                             .FirstOrDefault();

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(result,value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task UpdateSharePriceForASymbol()
        {
            // Arrange
            var rateList = new List<HourlyShareRate>{new HourlyShareRate{Id = 1,Symbol = "CBI",Rate = 310.0M,TimeStamp = new DateTime(2017, 08, 17, 5, 0, 0)}};

            const string symbol = "CBI";

            // Mock object Return
            _shareRepositoryMock
                .Setup(mock => mock.Query())
                .Returns(new AsyncQueryResult<HourlyShareRate>(rateList));

            // Act
            var result = await _shareService.UpdateLastPriceAsync(symbol, 310.0M);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(result, rateList.First());
        }

        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<HourlyShareRate> ArrangeRates()
        {
            var rates = new[]
            {
                new HourlyShareRate
                {
                    Id = 1,
                    Symbol = "CBI",
                    Rate = 310.0M,
                    TimeStamp = new DateTime(2017, 08, 17, 5, 0, 0)
                },
                new HourlyShareRate
                {
                    Id = 2,
                    Symbol = "CBI",
                    Rate = 320.0M,
                    TimeStamp = new DateTime(2018, 08, 16, 5, 0, 0)
                },
                new HourlyShareRate
                {
                    Id = 3,
                    Symbol = "REL",
                    Rate = 300.0M,
                    TimeStamp = new DateTime(2018, 08, 17, 5, 0, 0)
                },
                new HourlyShareRate
                {
                    Id = 4,
                    Symbol = "CBI",
                    Rate = 300.0M,
                    TimeStamp = new DateTime(2018, 08, 17, 5, 0, 0)
                },
                new HourlyShareRate
                {
                    Id = 5,
                    Symbol = "CBI",
                    Rate = 400.0M,
                    TimeStamp = new DateTime(2018, 08, 17, 6, 0, 0)
                },
                new HourlyShareRate
                {
                    Id = 6,
                    Symbol = "IBM",
                    Rate = 300.0M,
                    TimeStamp = new DateTime(2018, 08, 17, 5, 0, 0)
                },
            };

            return rates.ToList();
        }
    }
}
