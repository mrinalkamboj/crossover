﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;
using XOProject.Services.Domain;

namespace XOProject.Services.Exchange
{
    /// <summary>
    /// Analytics Service, for Daily, Weekly and Monthly Analytics
    /// </summary>
    public class AnalyticsService : IAnalyticsService
    {
        /// <summary>
        /// Share Repository to fetch the Share Data
        /// </summary>
        private readonly IShareRepository _shareRepository;

        /// <summary>
        /// Analytics Service Constructor
        /// </summary>
        /// <param name="shareRepository"></param>
        public AnalyticsService(IShareRepository shareRepository)
        {
            _shareRepository = shareRepository;
        }

        /// <summary>
        /// Daily Data Service
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        public async Task<AnalyticsPrice> GetDailyAsync(string symbol, DateTime day)
        {
            var hourlyShareRateList = await _shareRepository.Query() // Query the Repository to fetch the IQueryable<HourlyShareRate>
                                                            .Where(hsr => hsr.Symbol.Equals(symbol)) // Filter by Symbol
                                                            .Where(hsr => hsr.TimeStamp.Year.Equals(day.Year) && // filter by DateTime Year/Month/Date
                                                                          hsr.TimeStamp.Month.Equals(day.Month) &&
                                                                          hsr.TimeStamp.Day.Equals(day.Day))
                                                            .ToListAsync();

            var hourlyShareRateOrderByTime = hourlyShareRateList.OrderBy(hsr => hsr.TimeStamp).ToList(); //Ordering by time to get Open and close price points
            var hourlyShareRateOrderByRate = hourlyShareRateList.OrderBy(hsr => hsr.Rate).ToList(); // Ordering by Rate to get high and low price

            // Create and return AnalyticsPrice data from the information created above
            return new AnalyticsPrice
            {
                Open = hourlyShareRateOrderByTime.FirstOrDefault()?.Rate ?? 0.0M,
                Close = hourlyShareRateOrderByTime.LastOrDefault()?.Rate ?? 0.0M,
                Low = hourlyShareRateOrderByRate.FirstOrDefault()?.Rate ?? 0.0M,
                High = hourlyShareRateOrderByRate.LastOrDefault()?.Rate ?? 0.0M,
            };
        }

        /// <summary>
        /// Weekly Data Service
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="year"></param>
        /// <param name="week"></param>
        /// <returns></returns>
        public async Task<AnalyticsPrice> GetWeeklyAsync(string symbol, int year, int week)
        {
            var weeklyShareRateList = await _shareRepository.Query() // Query the Repository to fetch the IQueryable<HourlyShareRate>
                                                            .Where(hsr => hsr.Symbol.Equals(symbol)) // Filter by Symbol
                                                            .Where(hsr => hsr.TimeStamp.Year.Equals(year) && // filter by DateTime Year/Week
                                                                          GetWeekNumber(hsr.TimeStamp).Equals(week)) // Fetching the Week Number
                                                            .ToListAsync();

            var weeklyShareRateOrderByTime = weeklyShareRateList.OrderBy(hsr => hsr.TimeStamp).ToList(); //Ordering by time to get Open and close price points
            var weeklyShareRateOrderByRate = weeklyShareRateList.OrderBy(hsr => hsr.Rate).ToList(); // Ordering by Rate to get high and low price

            // Create and return AnalyticsPrice data from the information created above
            return new AnalyticsPrice
            {
                Open = weeklyShareRateOrderByTime.FirstOrDefault()?.Rate ?? 0.0M,
                Close = weeklyShareRateOrderByTime.LastOrDefault()?.Rate ?? 0.0M,
                Low = weeklyShareRateOrderByRate.FirstOrDefault()?.Rate ?? 0.0M,
                High = weeklyShareRateOrderByRate.LastOrDefault()?.Rate ?? 0.0M,
            };
        }

        /// <summary>
        /// Monthly Data Service
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public async Task<AnalyticsPrice> GetMonthlyAsync(string symbol, int year, int month)
        {
            var monthlyShareRateList = await _shareRepository.Query() // Query the Repository to fetch the IQueryable<HourlyShareRate>
                .Where(hsr => hsr.Symbol.Equals(symbol)) // Filter by Symbol
                .Where(hsr => hsr.TimeStamp.Year.Equals(year) && // filter by DateTime Year/Month
                              hsr.TimeStamp.Month.Equals(month))
                .ToListAsync();

            var monthlyShareRateOrderByTime = monthlyShareRateList.OrderBy(hsr => hsr.TimeStamp).ToList(); //Ordering by time to get Open and close price points
            var monthlyShareRateOrderByRate = monthlyShareRateList.OrderBy(hsr => hsr.Rate).ToList(); // Ordering by Rate to get high and low price

            // Create and return AnalyticsPrice data from the information created above
            return new AnalyticsPrice
            {
                Open = monthlyShareRateOrderByTime.FirstOrDefault()?.Rate ?? 0.0M,
                Close = monthlyShareRateOrderByTime.LastOrDefault()?.Rate ?? 0.0M,
                Low = monthlyShareRateOrderByRate.FirstOrDefault()?.Rate ?? 0.0M,
                High = monthlyShareRateOrderByRate.LastOrDefault()?.Rate ?? 0.0M,
            };
        }

        /// <summary>
        /// Fetch Week Number using Current Culture and CalendarWeekRule.FirstDay rule
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private int GetWeekNumber(DateTime dateTime)
        {
            var currentCultureInfo = CultureInfo.CurrentCulture;
            var weekNum = currentCultureInfo.Calendar.GetWeekOfYear(dateTime, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            return weekNum;
        }
    }
}