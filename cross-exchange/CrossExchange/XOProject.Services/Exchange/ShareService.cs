﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XOProject.Repository.Domain;
using XOProject.Repository.Exchange;

namespace XOProject.Services.Exchange
{
    public class ShareService : GenericService<HourlyShareRate>, IShareService
    {
        public ShareService(IShareRepository shareRepository) : base(shareRepository){ }

        public async Task<IList<HourlyShareRate>> GetBySymbolAsync(string symbol)
        {
            return await EntityRepository
                        .Query()
                        .Where(x => x.Symbol.Equals(symbol))
                        .Distinct() // Fixing the Duplicate Records for the Hourly Rate using IEquatable<HourlyShareRate>
                        .ToListAsync(); // Bug-Fix Issue: 1. The team noticed that sometimes there are duplicate records for the hourly rate.
        }

        public async Task<HourlyShareRate> GetHourlyAsync(string symbol, DateTime dateAndHour)
        {
            var date = dateAndHour.Date;
            var hour = dateAndHour.Hour;

            return await EntityRepository
                        .Query()
                        .Where(x => x.Symbol.Equals(symbol)
                               && x.TimeStamp.Date == date
                               && x.TimeStamp.Hour == hour)
                        .Distinct() // Remove Duplicate Records
                        .FirstOrDefaultAsync();
        }

        public async Task<HourlyShareRate> GetLastPriceAsync(string symbol)
        {
            var share = await EntityRepository
                .Query()
                .Where(x => x.Symbol.Equals(symbol))
                .Distinct() // Remove Duplicate Records
                .OrderByDescending(x => x.TimeStamp)
                .FirstOrDefaultAsync();
            return share;
        }

        public async Task<HourlyShareRate> UpdateLastPriceAsync(string symbol, decimal rate)
        {
            // Usig the existing method. Ordering shall be using the TimeStamp instead of Rate for Last price
            // Call the already implemented, GetLastPriceAsync
            var share = await GetLastPriceAsync(symbol); // Bug-Fix - 2. The team identified shares with unexpected rates which do not match with data from other sources.
            
            if (share == null)
            {
                return null;
            }

            share.Rate = rate;
            await EntityRepository.UpdateAsync(share);

            return share;
        }
    }
}
