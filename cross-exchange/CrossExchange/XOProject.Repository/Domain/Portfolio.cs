﻿using System;
using System.Collections.Generic;

namespace XOProject.Repository.Domain
{
    public class Portfolio : IEquatable<Portfolio>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public List<Trade> Trades { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="portfolio"></param>
        /// <returns></returns>
        public bool Equals(Portfolio portfolio)
        {
            return portfolio.Id.Equals(Id)
                   && portfolio.Name.Equals(Name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="portfolio"></param>
        /// <returns></returns>
        public override bool Equals(object portfolio)
        {
            return Equals(portfolio as Portfolio);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Id.GetHashCode();
                hash = hash * 23 + Name.GetHashCode();

                return hash;
            }
        }
    }
}