﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace XOProject.Repository.Domain
{
    public class Trade : IEquatable<Trade>
    {
        public int Id { get; set; }
        
        public string Symbol { get; set; }

        public int NoOfShares { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal ContractPrice { get; set; }

        public int PortfolioId { get; set; }
        
        public string Action { get; set; }

        public bool Equals(Trade trade)
        {
            return trade.Id.Equals(Id)
                   && trade.Symbol.Equals(Symbol)
                   && trade.NoOfShares.Equals(NoOfShares)
                   && trade.ContractPrice.Equals(ContractPrice)
                   && trade.PortfolioId.Equals(PortfolioId)
                   && trade.Action.Equals(Action);
        }

        public override bool Equals(object trade)
        {
            return Equals(trade as Trade);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Id.GetHashCode();
                hash = hash * 23 + Symbol.GetHashCode();
                hash = hash * 23 + NoOfShares.GetHashCode();
                hash = hash * 23 + ContractPrice.GetHashCode();
                hash = hash * 23 + PortfolioId.GetHashCode();
                hash = hash * 23 + Action.GetHashCode();

                return hash;
            }
        }
    }
}