﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace XOProject.Repository.Domain
{
    public class HourlyShareRate : IEquatable<HourlyShareRate>
    {
        public int Id { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Symbol { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Rate { get; set; }

        public bool Equals(HourlyShareRate hourlyShareRateModel)
        {
            return hourlyShareRateModel.Id.Equals(Id)
                   && hourlyShareRateModel.TimeStamp.Equals(TimeStamp)
                   && hourlyShareRateModel.Symbol.Equals(Symbol)
                   && hourlyShareRateModel.Rate.Equals(Rate);
        }

        public override bool Equals(object hourlyShareRateModel)
        {
            return Equals(hourlyShareRateModel as HourlyShareRate);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;

                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Id.GetHashCode();
                hash = hash * 23 + TimeStamp.GetHashCode();
                hash = hash * 23 + Symbol.GetHashCode();
                hash = hash * 23 + Rate.GetHashCode();

                return hash;
            }
        }
    }
}
