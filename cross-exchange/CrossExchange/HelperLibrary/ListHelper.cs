﻿using System;
using System.Collections.Generic;

namespace XOProject.Helpers
{
    public static class ListHelper
    {
        public static bool CompareList<T>(List<T> firstCollection, List<T> secondCollection)
        where T:IEquatable<T>
        {
            return firstCollection.TrueForAll(secondCollection.Contains) &&
                   secondCollection.TrueForAll(firstCollection.Contains);
        }
    }
}
