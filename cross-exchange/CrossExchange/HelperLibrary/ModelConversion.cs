﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XOProject.Api.Model;
using XOProject.Api.Model.Analytics;
using XOProject.Repository.Domain;
using XOProject.Services.Domain;

namespace XOProject.Helpers
{
    public static class ModelConversion
    {
        public static IList<HourlyShareRate> Map(IList<HourlyShareRateModel> rates)
        {
            return rates.Select(Map).ToList();
        }

        public static IList<Trade> Map(IList<TradeModel> rates)
        {
            return rates.Select(Map).ToList();
        }

        public static HourlyShareRate Map(HourlyShareRateModel rate)
        {
            return new HourlyShareRate()
            {
                Id = rate.Id,
                TimeStamp = rate.TimeStamp,
                Rate = rate.Rate,
                Symbol = rate.Symbol
            };
        }

        /// <summary>
        /// AnalyticsPrice to the PriceModel mapping
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public static PriceModel Map(AnalyticsPrice price)
        {
            return new PriceModel()
            {
                Open = price.Open,
                Close = price.Close,
                High = price.High,
                Low = price.Low
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="portfolio"></param>
        /// <returns></returns>
        public static PortfolioModel Map(Portfolio portfolio)
        {
            return new PortfolioModel()
            {
                Id = portfolio.Id,
                Name = portfolio.Name
            };
        }

        public static Trade Map(TradeModel tradeModel)
        {
            return new Trade
            {
                Symbol = tradeModel.Symbol,
                PortfolioId = tradeModel.PortfolioId,
                Action = tradeModel.Action,
                NoOfShares = tradeModel.NoOfShares
            };
        }

        
    }
}
